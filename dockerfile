FROM python:latest
WORKDIR /usr.src.app
COPY helloworld.py /usr.src.app
CMD [ "python", "helloworld.py" ]
